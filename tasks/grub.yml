---

- name: set list of init hooks
  set_fact:
    initHooks:
      - base
      - udev
      - autodetect
      - modconf
      - keyboard
      - keymap
      - block
      - encrypt
      - resume
      - filesystems

- name: add resume HOOK in mkinitcpio.conf
  lineinfile:
    path: /etc/mkinitcpio.conf
    regexp: "^HOOKS="
    line: "HOOKS=({{ initHooks | join(' ') }})"
  register: mkinitcpio_conf

- name: update mkinitcpio
  shell:
    cmd: mkinitcpio -p linux
  when: mkinitcpio_conf is changed

- name: find swapfile offset on disk
  shell: 'filefrag -v /swapfile | grep -E "^ +0" | tr -s " ." "::" | cut -d: -f5'
  check_mode: no
  ignore_errors: true
  changed_when: false
  register: swapfileFilefrag

- name: set swapfile variable
  set_fact:
    swapfileOffset: '{{swapfileFilefrag.stdout}}'

- name: find / device name
  set_fact:
    swapDevice: '{{ item.device }}'
  with_items: '{{ansible_mounts}}'
  when: item.mount == "/"

- name: set cryptdevice variable
  set_fact:
    cryptdevice: 'cryptdevice={{ ansible_cmdline.cryptdevice }}'
  when: ansible_cmdline.cryptdevice is defined

- block:

  - name: find cryptdevice
    shell:
      cmd: readlink -f $(echo '{{ ansible_cmdline.cryptdevice }}' | sed "s/:.*$//") | sed 's/[0-9]*$//'
    check_mode: no
    ignore_errors: true
    changed_when: false
    failed_when: false
    register: rootDeviceReadlink

  - name: find root device when encrypted
    set_fact:
      rootDevice: '{{ rootDeviceReadlink.stdout }}'

  when: ansible_cmdline.cryptdevice is defined

- block:

  - name: find /boot device
    shell:
      cmd: df -h /boot | tail -n +2 | head -1 | sed "s/ .*//" | sed 's/[0-9]*$//'
    check_mode: no
    ignore_errors: true
    changed_when: false
    failed_when: false
    register: rootDeviceDf

  - name: find /boot device when not encrypted
    set_fact:
      rootDevice: '{{ rootDeviceDf.stdout }}'

  when: ansible_cmdline.cryptdevice is not defined

- name: template grub config file
  template:
    src: grub
    dest: /etc/default/
  register: grub_default

- block:

  - name: grub mkconfig
    shell:
      cmd: grub-mkconfig -o /boot/grub/grub.cfg

  - name: grub install
    shell:
      cmd: grub-install {{ rootDevice }}

  when: grub_default is changed
